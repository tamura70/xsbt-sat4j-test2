package web

case class TrailData(lit: Int, level: Int, reason: Seq[Int], learnt: Boolean)

class Trace {
  var trace: Seq[(String,Option[Seq[TrailData]])] = Seq.empty

  def addTrace(message: String): Unit =
    trace :+= (message, None)
  def addTrace(message: String, trail: Seq[TrailData]): Unit =
    trace :+= (message, Some(trail))

  def size = trace.size

  def getTrace(i: Int): Map[String,String] = trace(i) match {
    case (message, None) =>
      Map("message" -> message)
    case (message, Some(trail)) =>
      Map("message" -> message, "graph" -> getGraph(trail))
  }

  def getGraph(trail: Seq[TrailData]): String = {
    // TODO
    val dg = new Digraph
    // conflictFound: 2[F] 3[F] 1[F] , 2, 3
    dg.addNodeOpt("Conflict", Map("tooltip" -> "2 3 1"))
    dg.setLevel("Conflict", 2)
    dg.addArc("-2", "Conflict")
    dg.addArc("-3", "Conflict")
    dg.addArc("-1", "Conflict")
    // trail(0) = -1, reason = null
    dg.addNodeOpt("-1", Map("tooltip" -> "null"))
    dg.setLevel("-1", 1)
    // trail(1) = -3, reason = null
    dg.addNodeOpt("-3", Map("tooltip" -> "null"))
    dg.setLevel("-3", 2)
    // trail(2) = -2, reason = -2[T] 3[F] 1[F]
    dg.addNodeOpt("-2", Map("tooltip" -> "-2 3 1"))
    dg.setLevel("-2", 2)
    dg.addArc("-3", "-2")
    dg.addArc("-1", "-2")
    //
    dg.toGraphviz()
  }
}
